#!/usr/bin/env python3

import sys

sys.path.append(".")

from blueprints_ci import (  # noqa
    logger,
    Settings,
    send_testjob_request_to_squad,
    resolve_os_or_firmware,
    generate_lava_job_definition,
    register_callback_in_squad,
    resolve_test_plan,
)

#
#   Required environment variables
#
required_vars = [
    # Details to send results to SQUAD
    "SQUAD_HOST",
    "SQUAD_TOKEN",
    "SQUAD_GROUP",
    "SQUAD_PROJECT",
]


def write_testenv(settings, testenv):
    with open(f"{settings.CI_PROJECT_DIR}/test.env", "w") as fp:
        for env, value in testenv.items():
            fp.write(f"{env}={value}\n")


#
#   Test implementation
#
def test(settings):
    """
        1. Resolve both OS and FIRMWARE images
        2. Combine the images into a single one using
        3. Generate a LAVA job definition
        4. Submit it ledge.v.l.o via SQUAD
        5. Write useful information to ${CI_PROJECT_DIR}/test.env to pass to check stage
          - SQUAD_JOB_ID: returned when submitting testjob to SQUAD
    """
    # Let SQUAD aware if build passed or failed
    logger.info(f"Triggering tests for {settings.SQUAD_HOST}/{settings.SQUAD_GROUP}/{settings.SQUAD_PROJECT}/build/{settings.SQUAD_BUILD}")

    if settings.OS_BUILD_JOB_ID is None and settings.FIRMWARE_BUILD_JOB_ID is None:
        logger.warning("The following environment variables are missing: 'OS_BUILD_JOB_ID' or 'FIRMWARE_BUILD_JOB_ID'")
        return False

    if settings.OS_RESULT_IMAGE_FILE is None and settings.FIRMWARE_RESULT_IMAGE_FILE is None:
        logger.warning("The following environment variables are missing: 'OS_RESULT_IMAGE_FILE' or 'FIRMWARE_RESULT_IMAGE_FILE'")
        return False

    # Gather details on job type
    context = {}

    base_os_artifacts_url = f"{settings.CI_PROJECT_URL}/-/jobs/{settings.OS_BUILD_JOB_ID}/artifacts/raw/{settings.IMAGES_DIR}"
    base_firmware_artifacts_url = f"{settings.CI_PROJECT_URL}/-/jobs/{settings.FIRMWARE_BUILD_JOB_ID}/artifacts/raw/{settings.IMAGES_DIR}"

    if settings.RUNNING_NIGHTLY:
        logger.info(f"Running nightly-build tests using OS ({settings.IMAGES_DIR}/{settings.OS}) and FIRMWARE ({settings.IMAGES_DIR}/{settings.FIRMWARE})")

        # Nightly-build tests will have filename or URL as OS and FIRMWARE
        context["firmware_url"] = resolve_os_or_firmware(settings, settings.FIRMWARE, base_url=base_firmware_artifacts_url)
        context["os_url"] = resolve_os_or_firmware(settings, settings.OS, base_url=base_os_artifacts_url)

    else:
        logger.info(f"Resolving OS ({settings.OS}) or FIRMWARE ({settings.FIRMWARE})")

        if settings.FIRMWARE is not None:
            # Testing an OS
            context["os_url"] = resolve_os_or_firmware(settings, settings.OS_RESULT_IMAGE_FILE, base_url=base_os_artifacts_url)
            context["firmware_url"] = resolve_os_or_firmware(settings, settings.FIRMWARE)

        elif settings.OS is not None:
            # Testing a FIRMWARE
            context["firmware_url"] = resolve_os_or_firmware(settings, settings.FIRMWARE_RESULT_IMAGE_FILE, base_url=base_firmware_artifacts_url)
            context["os_url"] = resolve_os_or_firmware(settings, settings.OS)

        else:
            logger.warning("Unexpected behavior: either $OS or $FIRMWARE should be defined")
            return False

    if context["os_url"] is None or context["firmware_url"] is None:
        return False

    logger.info(f"Figuring out what tests to run for {settings.LAVA_DEVICE}")
    context["test_plan"] = resolve_test_plan(settings)

    logger.info(f"Generating test job definition for {settings.LAVA_DEVICE} and {context}")
    definition = generate_lava_job_definition(settings, context=context)
    if definition is None:
        return False

    logger.info(definition)
    logger.info("Submitting test job definition to Squad/LAVA")

    ok, job_id = send_testjob_request_to_squad(settings, definition)
    if not ok:
        return False

    testenv_out = {
        "SQUAD_JOB_ID": job_id,
        "SQUAD_GROUP": settings.SQUAD_GROUP,
        "SQUAD_PROJECT": settings.SQUAD_PROJECT,
    }
    write_testenv(settings, testenv_out)

    logger.info("Registering callback so that next stage is triggered by Squad")
    result = register_callback_in_squad(settings)

    logger.info(f"Job has been triggered and is being watched. Once finished, Squad will trigger Gitlab's job \"check-{settings.CI_JOB_NAME}\", where test results and logs will be present")
    return result


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False
    return test(settings)


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
